﻿using System.ComponentModel.DataAnnotations;

namespace HRManagementClientSide.Models
{
    public class UserLoginViewModel
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
